/*

MongoDB Atlas?

MongoDB Atlas is a MongoDB database in the cloud. It is the cloud service of MongoDB, the leading NoSQL database.

What is Robo3T?
Robo3T is an application which allows the use of GUI to manipulate MongoDB databases. The advantage of Robo3T in just using Atlas is that Robo3T is a local application

What is Shell?
The shell is an interface in MongoDB that allows to input commands and perform CRUD on our database.

*/

//Create

/*

	db.collection.insertOne({

		"fieldA": "Value A",
		"fieldB": "Value B"

	}) - allows us to insert a document into a collection

	db.collection.insertMany([

		{
			"fieldA": "ValueA",
			"fieldB": "ValueB"
		},
		{
			"fieldA": "ValueA",
			"fieldB": "ValueB"
		},

	]) - allow us to insert multiple documents into a collection.

*/

//Read

//db.collection.find() 
//allow us to find/return all documents in a collection.

//db.collection.findOne({}) 
//allow us to find/return the first document in a collection.

//db.collection.find({"criteria":"value"})
//allow us to find/return all documents that matches the criteria in a collection.

//db.collection.findOne({"criteria":"value"})
//allow us to find/return the first document that matches the criteria in a collection.


//Update

//db.collection.updateOne({"criteria":"value"},{$set:{"fieldToBeUpdated":"Updated Value"}})
//allows us to update the first item that matches our criteria.

//db.collection.updateOne({},{$set:{"fieldToBeUpdated":"Updated Value"}})
//allows us to update the first item in the collection.

//db.collection.updateMany({"criteria":"value"},{$set:{"fieldToBeUpdated":"Updated Value"}})
//allows us to update all items that matches our criteria

//db.collection.updateMany({},{$set:{"fieldToBeUpdated":"Updated Value"}})
//allows us to update all items in the collection.


//Delete

//db.collection.deleteOne({"criteria":"value"})
//allows us to update the first item that matches our criteria.

//db.collection.deleteMany({"criteria": "value"})
//allows us to update all items that matches our criteria

//db.collection.deleteOne({})
//allows us to update the first item in the collection.

//db.collection.deleteMany({})
//allows us to update all items in the collection.

db.users.deleteMany({})
