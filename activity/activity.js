//Create
	//Backstreet Boys
		//Nick Carter
			db.users.insertOne({
		        "firstName":"Nick",
		        "lastName":"Carter",
		        "email":"1stBSB@gmail.com",
		        "password":"firstBSboy1",
		        "isAdmin":false
		    })

		//Kevin Richardson
			db.users.insertOne({
		        "firstName":"Kevin",
		        "lastName":"Richardson",
		        "email":"2ndBSB@gmail.com",
		        "password":"secondBSboy2",
		        "isAdmin":false
		    })

		//Brian Littrell
			db.users.insertOne({
		        "firstName":"Brian",
		        "lastName":"Littrell",
		        "email":"3rdBSB@gmail.com",
		        "password":"thirdBSboy3",
		        "isAdmin":false
		    })

		//AJ McLean
			db.users.insertOne({
		        "firstName":"AJ",
		        "lastName":"McLean",
		        "email":"4thBSB@gmail.com",
		        "password":"fourthBSboy4",
		        "isAdmin":false
		    })

		//Howie Dorough
			db.users.insertOne({
		        "firstName":"Howie",
		        "lastName":"Dorough",
		        "email":"5thBSB@gmail.com",
		        "password":"fifthBSboy5",
		        "isAdmin":false
		    })


	//Courses
		//Music 101
			db.courses.insertOne({
	        	"name":"Music 101",
	        	"price":4000,
	        	"isActive":false
	    	})	

	    //Theatre 101
	    	db.courses.insertOne({
	    	    "name":"Theatre 101",
	    	    "price":5000,
	    	    "isActive":false
	    	})

	    //Physical Education 101
	    	db.courses.insertOne({
	        	"name":"Physical Education 101",
	        	"price":3000,
	        	"isActive":false
	    	})


//Read
	//All Non-Admin Users
		db.users.find({"isAdmin":false})

//Update
	//Nick Carter as Admin
		db.users.updateOne({},{$set:{"isAdmin":true}})

	//Music 101 as Active
		db.courses.updateOne({},{$set:{"isActive":true}})

//Delete
	//All Inactive Courses
		db.courses.deleteMany({"isActive":false})
